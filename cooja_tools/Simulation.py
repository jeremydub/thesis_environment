import random
import shelve
import datetime
import os
import os.path
import shutil
from lxml import etree
import subprocess
import copy
import multiprocessing
import tempfile
import threading
import traceback

import cooja_tools
from Log import Log
from Mote import MoteType, Mote, CompilationError
from Topology import Topology

CONTIKI_PATH = "/home/user/contiki"
COOJA_PATH = CONTIKI_PATH+"/tools/cooja"

class CoojaSimulation:
	def __init__(self, topology, seed=None, success_ratio_tx=1.0, success_ratio_rx=1.0, title="Simulation",\
				 timeout=60,debug_info={}):
		
		if seed == None:
			self.seed=random.randint(0,1000000)
		else:
			self.seed=seed

		self.topology=copy.deepcopy(topology)

		self.success_ratio_rx=success_ratio_rx
		self.success_ratio_tx=success_ratio_tx

		self.title=title
		self.timeout=timeout

		now=datetime.datetime.today()
		self.id=str(now.year)+""+str(now.month).zfill(2)+""+str(now.day).zfill(2)+"_"+str(now.hour).zfill(2)\
				+""+str(now.minute).zfill(2)+""+str(now.second).zfill(2)\
				+"_"+hex(id(self))[2:]

		mote_types=[]

		for mote in topology:
			if mote.mote_type != None and mote.mote_type not in mote_types:
				mote_types.append(mote.mote_type)

		self.mote_types=mote_types

		# User-defined dictionary used for debugging or for later analysis
		self.debug_info=debug_info

		self.return_value=-1

	def run(self, log_file=None, pcap_file=None, filename_prefix=None, enable_log=True, \
				enable_pcap=True, remove_csc=True, verbose=False, folder="data", \
				notify_motes_when_finished=True):
		code = -1

		temp_dir = tempfile.gettempdir()+"/"+"cooja_sim_"+hex(id(self))[2:]+"/"
		os.makedirs(temp_dir)

		try:
			csc_filepath = temp_dir+str(self.id)+".csc"
			self.check_settings(verbose)
			self.export_as_csc(csc_filepath, enable_log, enable_pcap)
			absolute_path=os.path.abspath(csc_filepath)

			jar_location = COOJA_PATH+"/dist/"

			command="cd "+temp_dir+" && java -mx512m -jar "+jar_location+"cooja.jar -nogui="+absolute_path+" -contiki="+CONTIKI_PATH
			p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
			if verbose:
				terminal_width = int(subprocess.check_output(['stty', 'size']).split()[1])
				time_remaining = ""
				progress = "Starting..."
				for line in iter(p.stdout.readline, ''):
				    line = p.stdout.readline()

				    if not line :
				    	break
				    else:
				    	line = line.strip()
				    	if "%, done in " in line:
				    		parts = line.split(", done in ")
				    		time_remaining = " | "+ parts[1]
				    		progress = parts[0].split()[-1]
				    	elif "Test script finished" in line:
				    		progress = "100%"
				    		time_remaining = ""
				    	elif "Test script activated" in line:
				    		progress = "0%"
				    		time_remaining = ""
				    print (self.title+"   ["+progress+""+time_remaining+"]").ljust(terminal_width)+"\r",
				p.stdout.close()
				print ""
			code=p.wait()
			self.return_value=code

			if self.has_suceed():
				if folder != None and folder != "":
					if not os.path.exists(folder) and (log_file == None or pcap_file == None) :
						os.makedirs(folder)
					if folder[-1] != "/":
						folder += "/"
				else:
					folder = ""
				prefix = folder
				if filename_prefix != None:
					prefix += str(filename_prefix)
				if enable_log:
					if log_file == None:
						log_file = prefix+str(self.id)+".log"

					self.log_file=log_file
					os.rename(temp_dir+"COOJA.testlog",log_file)

				if enable_pcap:
					if pcap_file == None:
						pcap_file = prefix+str(self.id)+".pcap"
					origin = temp_dir+str(self.id)+".pcap"
					if os.path.exists(origin):
						self.pcap_file=pcap_file
						os.rename(origin,pcap_file)
			else:
				if os.path.exists(temp_dir+"/COOJA.log"):
					print "\033[38;5;1m"
					os.system("cat "+temp_dir+"/COOJA.log")
					print "\033[0m"
		except CompilationError as e:
			print e
		except SettingsError as e:
			print e
		except Exception as e:
			traceback.print_exc()
		finally:
			if remove_csc and self.has_suceed():
				os.remove(csc_filepath)

			if notify_motes_when_finished:
				# Notify mote types that simulation finished
				for mote_type in self.mote_types:
					mote_type.simulation_finished()

			shutil.rmtree(temp_dir)

			return code

	def run_with_cooja(self):
		temp_folder = tempfile.gettempdir()+"/"+"cooja_sim_"+hex(id(self))[2:]+"/"

		try:
			self.export(temp_folder, gui_enabled=True, verbose=True)
			absolute_path=os.path.abspath(temp_folder+"/simulation.csc")
			command="cd "+COOJA_PATH+"/dist && java -mx512m"+\
					" -jar cooja.jar -quickstart="+absolute_path+\
					" 2> /dev/null > /dev/null"
			os.system(command)
		except CompilationError as e:
			print e
		except SettingsError as e:
			print e
		finally:
			shutil.rmtree(temp_folder)

	def check_settings(self,verbose=False):
		errors = []
		for mote in self.topology:
			if mote.mote_type == None:
				errors.append("Mote #"+str(mote.id)+" has no mote type")

		# Check mote type and firmware
		for mote_type in self.mote_types:
			compiled = mote_type.compile_firmware(verbose=verbose)
			if not compiled:
				errors.append("Firmware '"+str(mote_type.firmware_path)+"' did not compiled correctly")
			# If compiling did not work
			if not mote_type.firmware_exists():
				errors.append("Firmware '"+str(mote_type.firmware_path)+"' does not exist")

		if len(errors) > 0:
			raise SettingsError("\n".join(errors))

	def has_suceed(self):
		return self.return_value==0

	def get_log_filepath(self):
		if self.has_suceed:
			return self.log_file
		else:
			return None

	def get_pcap_filepath(self):
		if self.has_suceed:
			return self.pcap_file
		else:
			return None

	def export(self, folder=None, gui_enabled=False, verbose=False):
		if folder == None:
			folder = "simulation_"+str(self.id)

		if folder[-1] == "/":
			folder = folder[:-1]

		if not os.path.exists(folder):
			os.makedirs(folder)

		self.check_settings(verbose=verbose)

		sim = copy.deepcopy(self)

		if verbose:
			print "Exporting simulation in '"+folder+"/' ..."

		i=0
		for i in range(len(self.mote_types)):
			firmware_filename = self.mote_types[i].firmware_path.split("/")[-1]
			new_path = folder+"/"+firmware_filename
			self.mote_types[i].save_firmware_as(new_path, verbose=verbose)
			sim.mote_types[i].firmware_path="[CONFIG_DIR]/"+firmware_filename
		print "Saving simulation as Cooja file (.csc)"
		sim.export_as_csc(folder+"/simulation.csc", gui_enabled=gui_enabled)

	def export_as_csc(self, filename, enable_log=True, enable_pcap=True, gui_enabled=False):
		"""
		Simple and primitive .csc file exporter.
		"""
		xb = XmlBuilder()

		xb.write('<?xml version="1.0" encoding="UTF-8"?>')
		xb.write("<simconf>")
		xb.indent()
		xb.write('<project EXPORT="discard">[APPS_DIR]/mrm</project>')
		xb.write('<project EXPORT="discard">[APPS_DIR]/mspsim</project>')
		xb.write('<project EXPORT="discard">[APPS_DIR]/avrora</project>')
		xb.write('<project EXPORT="discard">[APPS_DIR]/serial_socket</project>')
		
		xb.write("<simulation>")
		xb.indent()

		# General settings
		xb.write('<title>'+self.title+'</title>')
		xb.write('<randomseed>'+str(self.seed)+'</randomseed>')
		xb.write('<motedelay_us>'+str(1000000)+'</motedelay_us>')

		# Radio settings
		xb.write('<radiomedium>')
		xb.indent()
		xb.write('se.sics.cooja.radiomediums.UDGM')
		xb.write('<transmitting_range>'+str(self.topology.transmitting_range)+'</transmitting_range>')
		xb.write('<interference_range>'+str(self.topology.interference_range)+'</interference_range>')
		xb.write('<success_ratio_tx>'+str(self.success_ratio_tx)+'</success_ratio_tx>')
		xb.write('<success_ratio_rx>'+str(self.success_ratio_rx)+'</success_ratio_rx>')
		xb.unindent()
		xb.write('</radiomedium>')

		xb.write('<events>')
		xb.indent()
		xb.write('<logoutput>40000</logoutput>')
		xb.unindent()
		xb.write('</events>')

		# Mote Types
		for mote_type in self.mote_types:
			mote_type.to_xml(xb)

		# Motes
		for mote in self.topology:
			mote.to_xml(xb)

		xb.unindent()
		xb.write("</simulation>")

		if not gui_enabled:
			xb.write("<plugin>")
			xb.indent()
			xb.write("org.contikios.cooja.plugins.ScriptRunner")
			xb.write("<plugin_config>")
			xb.indent()
			xb.write("<script>&#xD;")
			xb.indent()
			xb.write("TIMEOUT("+str(1000*self.timeout)+",log.testOK());&#xD;")
			xb.write("while (true) {&#xD;")
			xb.write("log.log(time + \":\" + id + \":\" + msg + \"\\n\");&#xD;")
			xb.write("YIELD();&#xD;")
			xb.write("}</script>")
			xb.unindent()
			xb.write("<active>true</active>")
			xb.unindent()
			xb.write("</plugin_config>")
			xb.write("<width>600</width>")
			xb.write("<z>0</z>")
			xb.write("<height>700</height>")
			xb.write("<location_x>1120</location_x>")
			xb.write("<location_y>180</location_y>")
			xb.unindent()
			xb.write("</plugin>")

		if gui_enabled:
			xb.write("<plugin>")
			xb.indent()
			xb.write("org.contikios.cooja.plugins.LogListener")
			xb.write("<plugin_config>")
			xb.indent()
			xb.write("<filter />")
			xb.write("<formatted_time />")
			xb.write("<coloring />")
			xb.unindent()
			xb.write("</plugin_config>")
			xb.write("<width>740</width>")
			xb.write("<z>2</z>")
			xb.write("<height>1004</height>")
			xb.write("<location_x>545</location_x>")
			xb.write("<location_y>0</location_y>")
			xb.unindent()
			xb.write("</plugin>")

			xb.write("<plugin>")
			xb.indent()
			xb.write("org.contikios.cooja.plugins.RadioLogger")
			xb.write("<plugin_config>")
			xb.indent()
			xb.write("<split>640</split>")
			xb.write("<formatted_time />")
			xb.write("<showdups>false</showdups>")
			xb.write("<hidenodests>true</hidenodests>")
			xb.write("<analyzers name=\"6lowpan\" />")
			xb.unindent()
			xb.write("</plugin_config>")
			xb.write("<width>633</width>")
			xb.write("<z>1</z>")
			xb.write("<height>1011</height>")
			xb.write("<location_x>1287</location_x>")
			xb.write("<location_y>0</location_y>")
			xb.unindent()
			xb.write("</plugin>")

			xb.write("<plugin>")
			xb.indent()
			xb.write("org.contikios.cooja.plugins.Visualizer")
			xb.write("<plugin_config>")
			xb.indent()
			xb.write("<moterelations>true</moterelations>")
			xb.write("<skin>org.contikios.cooja.plugins.skins.TrafficVisualizerSkin</skin>")
			xb.write("<skin>org.contikios.cooja.plugins.skins.IDVisualizerSkin</skin>")
			xb.write("<skin>org.contikios.cooja.plugins.skins.UDGMVisualizerSkin</skin>")
			xb.write("<skin>org.contikios.cooja.plugins.skins.MoteTypeVisualizerSkin</skin>")
			xb.unindent()
			xb.write("</plugin_config>")
			xb.write("<width>541</width>")
			xb.write("<z>3</z>")
			xb.write("<height>824</height>")
			xb.write("<location_x>1</location_x>")
			xb.write("<location_y>1</location_y>")
			xb.unindent()
			xb.write("</plugin>")

			xb.write("<plugin>")
			xb.indent()
			xb.write("org.contikios.cooja.plugins.SimControl")
			xb.write("<width>516</width>")
			xb.write("<z>0</z>")
			xb.write("<height>159</height>")
			xb.write("<location_x>13</location_x>")
			xb.write("<location_y>839</location_y>")
			xb.unindent()
			xb.write("</plugin>")

		if not gui_enabled:
			if enable_pcap:
				xb.write("<plugin>")
				xb.indent()
				xb.write("org.contikios.cooja.plugins.PcapLogger")
				xb.write("<plugin_config>")
				xb.indent()
				xb.write("<destination_file>"+str(self.id)+".pcap"+"</destination_file>")
				xb.unindent()
				xb.write("</plugin_config>")
				xb.write("<width>500</width>")
				xb.write("<z>0</z>")
				xb.write("<height>100</height>")
				xb.write("<location_x>46</location_x>")
				xb.write("<location_y>48</location_y>")
				xb.unindent()
				xb.write("</plugin>")

		xb.unindent()
		xb.write("</simconf>")
		xb.write("")

		xb.save(filename)

	@staticmethod
	def from_csc(file_path, timeout=60, preserve_mote_types=True):
		"""
		Simple .csc file parser.
		"""
		folder = "/".join(file_path.split('/')[:-1])
		mote_types = []
		motes = []

		tree = etree.parse(file_path)

		# Parsing general simulation and radio settings
		simulation_tag=tree.xpath("/simconf/simulation")[0]
		title = str(simulation_tag.xpath("title/text()")[0])
		seed = str(simulation_tag.xpath("randomseed/text()")[0])
		radiomedium_tag = simulation_tag.find("radiomedium")
		success_ratio_rx = float(radiomedium_tag.xpath("success_ratio_rx/text()")[0])
		success_ratio_tx = float(radiomedium_tag.xpath("success_ratio_tx/text()")[0])

		# Creating topology
		topology = Topology.from_csc(file_path,preserve_mote_types)

		# Creating simulation
		simulation = CoojaSimulation(topology=topology, seed=seed,title=title,\
						success_ratio_tx=success_ratio_tx, timeout=timeout,\
						success_ratio_rx=success_ratio_rx)
		return simulation

	def __repr__(self):
		duration = str(self.timeout)
		if self.timeout / 60 == 0:
			duration += "sec"
		else :
			duration = str(self.timeout/60) + "min"
		return "\""+self.title+"\" : "+str(len(self.topology))+" motes, duration="+duration+", RX="+str(self.success_ratio_rx)+", TX="+str(self.success_ratio_tx)


class SimulationWorker:
	def __init__(self, n_thread=None, callback=None):
		if n_thread == None:
			n_thread = multiprocessing.cpu_count()
		self.n_thread = n_thread
		self.callback = callback
		self.simulations = []

		# dictionary that associate a simulation id to its run() arguments
		self.id_to_args={}

	def add_simulation(self,simulation, run_args={}):
		if isinstance(simulation,CoojaSimulation):
			self.simulations.append(simulation)

			if type(run_args) == dict:
				if not run_args.has_key('notify_motes_when_finished'):
					run_args['notify_motes_when_finished'] = False

			self.id_to_args[id(simulation)] = run_args

	def run(self, verbose=True):

		# Check settings and compile firmware before threads start
		for simulation in self.simulations:
			simulation.check_settings(verbose=verbose)

		self.simulations.sort(lambda x,y : int(10.**6*len(y.topology)/y.timeout) - int(10.**6*len(x.topology)/x.timeout))

		simulations_per_thread = []
		for i in range(self.n_thread):
			simulations_per_thread.append([])

		i = 0
		for simulation in self.simulations:
			simulations_per_thread[i%self.n_thread].append(simulation)
			i += 1

		threads=[]

		def target(thread_number):
			for simulation in simulations_per_thread[thread_number]:
				kwargs=self.id_to_args[id(simulation)]
				simulation.run(verbose=False, **kwargs)
				if self.callback != None:
					self.callback(simulation)

		for i in range(self.n_thread):
			thread = threading.Thread(target=target, args=(i,))
			threads.append(thread)

		for thread in threads:
			thread.start()

		for thread in threads:
			thread.join()

		for simulation in self.simulations:
			# notify mote types that simulation ended
			for mote_type in simulation.mote_types:
				mote_type.simulation_finished()

			if self.callback != None:
				self.callback(simulation)

		self.simulations = []

class SettingsError(Exception):
	pass

class SimulationsDatabase:
	def __init__(self, filename="simulations.db"):

		self.filename=filename

		db = shelve.open(self.filename)
		if 'simulations' not in db:
			db['simulations'] = []

		self.simulations=db['simulations']
		db.close()

	def add_simulation(self, sim):
		self.simulations.append(sim)

	def merge_db(self, db):
		merged_sim = self.simulations + db.simulations
		self.simulations=merged_sim

	def get_simulation_with_id(self, sim_id):
		return self.simulations[sim_id]

	def get_simulations(self):
		return self.simulations

	def commit_changes(self):
		db = shelve.open(self.filename)
		db['simulations']=self.simulations
		db.close()

	def query(self,id=None, n=None, title=None,topology=None,debug_info=None,success_ratio_rx=None,\
				success_ratio_tx=None,timeout=None,transmitting_range=None):
		
		if topology != None:
			n=None

		result=[]

		def is_dictionary_included(dic1,dic2):
			for key in dic1:
				if not(dic2.has_key(key)) or (dic2.has_key(key) and dic2[key]!=dic1[key]):
					return False
			return True

		for s in self.simulations:
			if (n == None or (n != None and n==len(s.topology)))\
				and (id == None or (nid != None and id==s.id))\
				and (title == None or (title != None and s.title.find(title)!=-1))\
				and (success_ratio_rx == None or (success_ratio_rx != None and success_ratio_rx==s.success_ratio_rx))\
				and (success_ratio_tx == None or (success_ratio_tx != None and success_ratio_tx==s.success_ratio_tx))\
				and (timeout == None or (timeout != None and timeout==s.timeout))\
				and (transmitting_range == None or (transmitting_range != None and transmitting_range==s.transmitting_range))\
				and (debug_info == None or (debug_info != None and is_dictionary_included(debug_info, s.debug_info)))\
				and (topology == None or (topology != None and topology==s.topology)) :
				result.append(s)
		return result

	def __len__(self):
		return len(self.simulations)

class XmlBuilder:
	indent_count=0
	indent_symbol="  "

	def __init__(self):
		self.content=[]

	def indent(self):
		self.indent_count += 1

	def unindent(self):
		self.indent_count -= 1

	def write(self, line):
		self.content.append((self.indent_symbol*self.indent_count)+line+'\n')

	def save(self, filename):
		f=open(filename,"w")
		for line in self.content:
			f.write(line)
		f.close()

	def __str__(self):
		return self.content

def check_cooja_settings():
		global CONTIKI_PATH
		global COOJA_PATH
		
		config_file = os.path.dirname(cooja_tools.__file__)+\
				"/cooja.properties"

		if not os.path.exists(config_file):
			f = open(config_file,'w')
			f.write("PATH_CONTIKI="+CONTIKI_PATH+"\n")
			f.write("CONFIGURED=0\n")
			f.close()
		
		content = []
		contiki_path = CONTIKI_PATH
		module_configured = 0

		f = open(config_file,'r')
		for line in f:
			if line.startswith("PATH_CONTIKI="):
				contiki_path=line.split("=")[1].strip()
				if contiki_path[0] != "/":
					contiki_path=os.path.abspath(contiki_path)
			if line.startswith("CONFIGURED="):
				module_configured=int(line.split("=")[1].strip())
			content.append(line)
		f.close()

		if contiki_path[-1] == "/":
			contiki_path = contiki_path[:-1]

		contiki_exists = os.path.exists(contiki_path+"/Makefile.include")

		if not contiki_exists:
			print "### COOJA MODULE CONFIGURATION :"
			while True:
				path=raw_input("Please enter absolute path to Contiki folder : ")
				if path[-1] == "/":
					path = path[:-1]
				if os.path.exists(path+"/Makefile.include"):
					contiki_path = path
					break
				else:
					print "That folder does not exist or is not a Contiki folder .."

		if not module_configured:
			folder_backup = contiki_path+"/tools/cooja_back"
			if not os.path.exists(folder_backup):
				print "### COOJA MODULE CONFIGURATION : Copying new Cooja version in '"+contiki_path+"/tools/cooja' and renamed old version to '"+contiki_path+"/tools/cooja_back'"
				os.rename(contiki_path+"/tools/cooja",folder_backup)
				new_cooja_folder = os.path.dirname(cooja_tools.__file__)+"/cooja"
				shutil.copytree(new_cooja_folder, contiki_path+"/tools/cooja")

		if not contiki_exists or not module_configured:

			f2 = open(config_file,'w')
			found_contiki_path = False
			found_configured = False
			for line in content:
				if line.startswith("PATH_CONTIKI="):
					f2.write("PATH_CONTIKI="+contiki_path+"\n")
					found_contiki_path = True
				elif line.startswith("CONFIGURED="):
					found_configured = True
					f2.write("CONFIGURED=1\n")
				else:
					f2.write(line)

			if not found_contiki_path:
				f2.write("PATH_CONTIKI="+contiki_path+"\n")
			if not found_configured:
				f2.write("CONFIGURED=1\n")
			f2.close()

		CONTIKI_PATH = contiki_path
		COOJA_PATH = CONTIKI_PATH+"/tools/cooja"

check_cooja_settings()
