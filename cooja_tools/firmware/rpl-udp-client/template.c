/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-udp-packet.h"
#include "sys/ctimer.h"
 #include "net/rpl/rpl.h"
 #include "node-id.h"
#include <stdio.h>
#include <string.h>

#define UDP_CLIENT_PORT 8765
#define UDP_SERVER_PORT 5678

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define SEND_MESSAGE 1
#define NB_SERVERS   1
#define NB_INSTANCES 1
#define START_DELAY		(30 * CLOCK_SECOND)
#define SEND_TIME		(random_rand() % (10 * CLOCK_SECOND))
#define MAX_PAYLOAD_LEN   50

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

static struct uip_udp_conn *client_conn;
/* addresses definition */

/*---------------------------------------------------------------------------*/
PROCESS(udp_client_process, "UDP client process");
AUTOSTART_PROCESSES(&udp_client_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  char *appdata;

  if(uip_newdata()) {
    appdata = (char *)uip_appdata;
    appdata[uip_datalen()] = 0;
    PRINTF("app:recv:%u:", UIP_IP_BUF->srcipaddr.u8[sizeof(UIP_IP_BUF->srcipaddr.u8) - 1]);
    PRINTF("%s\n", appdata);
  }
}
/*---------------------------------------------------------------------------*/
#if SEND_MESSAGE
static void
send_packet()
{
  static int seq_id;
  char buf[MAX_PAYLOAD_LEN];
  seq_id++;
  
  sprintf(buf, "Hello %d ! (via Instance %d)", seq_id, instance_ids[(seq_id-1)%NB_INSTANCES]);
  PRINTF("app:send:Hello %d ! (via Instance %d)\n", seq_id, instance_ids[(seq_id-1)%NB_INSTANCES]);
  client_conn->instance_id=instance_ids[(seq_id-1)%NB_INSTANCES];
  uip_udp_packet_sendto(client_conn, &buf, sizeof(buf),
                      &server_ipaddr[(seq_id-1)%NB_SERVERS], UIP_HTONS(UDP_SERVER_PORT));
}
#endif /* SEND_MESSAGE */
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Client IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
      /* hack to make address "final" */
      if (state == ADDR_TENTATIVE) {
	uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
      }
    }
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_client_process, ev, data)
{
  static struct etimer periodic;
  static int print = 0;
  unsigned long all_radio, all_time;

  PROCESS_BEGIN();

  PROCESS_PAUSE();

  PRINTF("UDP client process started\n");

  print_local_addresses();

#if SEND_MESSAGE
  /* new connection with remote host */
  client_conn = udp_new(NULL, UIP_HTONS(UDP_SERVER_PORT), NULL); 
  if(client_conn == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(client_conn, UIP_HTONS(UDP_CLIENT_PORT)); 

  PRINTF("Created a connection with the server ");
  PRINT6ADDR(&client_conn->ripaddr);
  PRINTF(" local/remote port %u/%u\n",
	UIP_HTONS(client_conn->lport), UIP_HTONS(client_conn->rport));

  /* addresses initialization */

#endif /* SEND_MESSAGE */

  /* authorized instances */

  etimer_set(&periodic, START_DELAY);

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }
    
    if(etimer_expired(&periodic)) {
      etimer_set(&periodic, SEND_TIME);
#if SEND_MESSAGE
      send_packet();
#endif /* SEND_MESSAGE */
      print++;
#if WITH_COMPOWER
      if (print == 1) {
        energest_flush();
        all_time = energest_type_time(ENERGEST_TYPE_CPU) + energest_type_time(ENERGEST_TYPE_LPM);
        all_radio = energest_type_time(ENERGEST_TYPE_LISTEN) +
          energest_type_time(ENERGEST_TYPE_TRANSMIT);
        printf("app:info:%lu:%lu\n",
         all_radio,
         all_time);
      }
      if (print == 3) {
  print = 0;
      }
#endif

      // After every 10 timer expirations, print some information
      /*if(print%2==0){
        print_local_addresses();
        rpl_print_neighbor_list();
      }*/
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
