import os
import Simulation
import shutil

class Mote:
	def __init__(self, id, x, y, mote_type=None, startup_delay=None):
		self.id = id
		
		self.x = x
		self.y = y
		self.z = 0

		self.nbr_count = 0

		self.mote_type = mote_type
		self.startup_delay = startup_delay

	def to_xml(self, xb):
		xb.write('<mote>')
		xb.indent()
		xb.write('<breakpoints />')
		self.interfaces_to_xml(xb)
		xb.write('<motetype_identifier>'+self.mote_type.identifier+'</motetype_identifier>')
		xb.unindent()
		xb.write('</mote>')

	def interfaces_to_xml(self, xb):
		xb.write('<interface_config>')
		xb.indent()
		xb.write('org.contikios.cooja.interfaces.Position')
		xb.write('<x>'+str(self.x)+'</x>')
		xb.write('<y>'+str(self.y)+'</y>')
		xb.write('<z>'+str(self.z)+'</z>')
		xb.unindent()
		xb.write('</interface_config>')

	def __str__(self):
		return '[#'+str(self.id)+']('+str(self.x)+', '+str(self.y)+')'

	def __repr__(self):
		return self.__str__()

	def __lt__(self, other):
		return self.nbr_count < other.nbr_count

	def __eq__(self, other):
		if type(self)==type(other):
			return self.x==other.x and self.y==other.y and self.z==other.z and self.id==other.id
		else:
			return False

class MoteType:
	class_counter = 0
	def __init__(self, firmware_path, identifier, java_class, make_target=None, interfaces=[], description=None, firmware_command=None):
		filename = firmware_path.split('/')[-1]
		if identifier == None:
			identifier = filename.split(".")[0]

		if description == None:
			description="Mote #"+identifier

		if make_target == None:
			make_target = filename

		if type(firmware_path) == str and firmware_path[0] != "/" and os.path.exists(firmware_path):
			firmware_path = os.path.abspath(firmware_path)

		self.identifier=identifier
		self.java_class=java_class
		self.firmware_path=firmware_path
		self.description = description
		self.make_target = make_target
		self.firmware_command = firmware_command

		common_interfaces=['org.contikios.cooja.interfaces.Position',\
			'org.contikios.cooja.interfaces.RimeAddress',\
			'org.contikios.cooja.interfaces.IPAddress',\
			'org.contikios.cooja.interfaces.Mote2MoteRelations',\
			'org.contikios.cooja.interfaces.MoteAttributes']

		self.interfaces=common_interfaces+interfaces

	def __str__(self):
		parts = self.firmware_path.split('/')
		folder = "/".join(parts[:-1])
		filename = parts[-1]
		return '['+self.identifier+': '+filename+']'

	def __repr__(self):
		return self.__str__()

	def to_xml(self, xb):
		xb.write('<motetype>')
		xb.indent()
		xb.write(self.java_class)
		xb.write('<identifier>'+self.identifier+'</identifier>')
		xb.write('<description>'+self.description+'</description>')
		xb.write('<firmware EXPORT="copy">'+self.firmware_path+'</firmware>')
		for interface in self.interfaces:
			xb.write('<moteinterface>'+interface+'</moteinterface>')
		xb.unindent()
		xb.write('</motetype>')

	def firmware_exists(self):
		return os.path.exists(self.firmware_path)

	def compile_firmware(self, make_options="", clean=False, verbose=False):
		parts = self.firmware_path.split('/')
		folder = "/".join(parts[:-1])
		filename = parts[-1]

		if self.check_makefile():
			if clean:
				os.system("cd "+folder+" && rm -f "+filename)
			
			error_file = "error_"+hex(id(self))[2:]+"_"+str(MoteType.class_counter)+".log"
			MoteType.class_counter += 1

			command = "cd "+folder+""
			if clean:
				command+=" && make clean 2> /dev/null > /dev/null"
			if verbose:
				print "Compiling Firmware for Mote Type "+repr(self)
			if self.firmware_command != None:
				command += " && " + self.firmware_command
			else:
				command += " && make "+self.make_target+" -j "+make_options
			command = command + " 2> "+error_file+" > /dev/null"
			code = os.system(command)
			if code != 0:
				if os.path.exists(folder+"/"+error_file):
					print "\033[38;5;1m"
					os.system("cat "+folder+"/"+error_file)
					print "\033[0m"
				raise CompilationError("An error occured during firmware compilation")
			os.remove(folder+"/"+error_file)
			return code == 0

		return self.firmware_exists()

	def check_makefile(self):
		"""
		Check contiki path in makefile
		"""
		parts = self.firmware_path.split('/')
		folder = "/".join(parts[:-1])
		makefile_path = folder+"/Makefile"

		valid = True

		if os.path.exists(makefile_path):
			f = open(makefile_path,'r')
			
			content = []

			for line in f:
				if line.startswith("CONTIKI="):
					contiki_path=line.split("=")[1].strip()
					if contiki_path[-1] != "/":
						contiki_path += "/"
					if contiki_path[0] != "/":
						contiki_path = folder+"/"+contiki_path
					if not os.path.exists(contiki_path+"Makefile.include"):
						valid = False
						content.append("CONTIKI="+Simulation.CONTIKI_PATH+"\n")
					else:
						content.append(line)
						break
				else:
					content.append(line)
			f.close()
			if not valid:
				os.rename(makefile_path, folder+"/Makefile.old")
				f2 = open(makefile_path,'w')

				for line in content:
					f2.write(line)
				f2.close()
		else:
			valid=False

		return valid

	def save_firmware_as(self, filepath, verbose=False):
		if not self.firmware_exists():
			result=self.compile_firmware(verbose=verbose)
			if result:
				shutil.copy2(self.firmware_path, filepath)
				self.simulation_finished()
		else:
			shutil.copy2(self.firmware_path, filepath)


	def simulation_finished(self):
		"""
		Function called when the simulation finished.
		"""

class CompilationError(Exception):
	pass