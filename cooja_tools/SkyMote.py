from Mote import *
import os
import cooja_tools
import shutil

class SkyMote(Mote):
	def __init__(self, id, x, y, mote_type=None, startup_delay=None):
		Mote.__init__(self, id, x, y, mote_type, startup_delay)

	def interfaces_to_xml(self, xb):
		Mote.interfaces_to_xml(self, xb)
		xb.write('<interface_config>')
		xb.indent()
		xb.write('org.contikios.cooja.mspmote.interfaces.MspMoteID')
		xb.write('<id>'+str(self.id)+'</id>')
		xb.unindent()
		xb.write('</interface_config>')
		if type(self.startup_delay) == int or type(self.startup_delay) == float :
			xb.write('<interface_config>')
			xb.indent()
			xb.write('org.contikios.cooja.mspmote.interfaces.MspClock')
			xb.write('<startup_delay_ms>'+str(int(self.startup_delay))+'</startup_delay_ms>')
			xb.unindent()
			xb.write('</interface_config>')

class SkyMoteType(MoteType):
	def __init__(self, firmware_path, identifier=None, make_target=None, description=None):
		interfaces=['org.contikios.cooja.mspmote.interfaces.MspClock',
		'org.contikios.cooja.mspmote.interfaces.MspMoteID',
		'org.contikios.cooja.mspmote.interfaces.SkyButton',
		'org.contikios.cooja.mspmote.interfaces.SkyFlash',
		'org.contikios.cooja.mspmote.interfaces.SkyCoffeeFilesystem',
		'org.contikios.cooja.mspmote.interfaces.Msp802154Radio',
		'org.contikios.cooja.mspmote.interfaces.MspSerial',
		'org.contikios.cooja.mspmote.interfaces.SkyLED',
		'org.contikios.cooja.mspmote.interfaces.MspDebugOutput',
		'org.contikios.cooja.mspmote.interfaces.SkyTemperature']
		MoteType.__init__(self, firmware_path, identifier, 'org.contikios.cooja.mspmote.SkyMoteType', make_target=make_target, interfaces=interfaces, description=description)

	def compile_firmware(self, make_options="", clean=False, verbose=False):
		return MoteType.compile_firmware(self, make_options="TARGET=sky", clean=clean, verbose=verbose)		

class AbstractSkyMoteType(SkyMoteType):
	def __init__(self, firmware_path, max_instances=2, max_mc_objects=1, enabled_metrics=None, \
			authorized_instances=[], make_target="", description=""):
		SkyMoteType.__init__(self, firmware_path, make_target=make_target, description=description)
		
		if enabled_metrics == None:
			enabled_metrics = [RPLInstance.METRIC_ETX, RPLInstance.METRIC_HOPCOUNT]

		self.max_instances = max_instances
		self.max_mc_objects = max_mc_objects
		self.enabled_metrics = enabled_metrics

		if type(authorized_instances) != list:
			raise TypeError("authorized_instances parameter must be a list of instance IDs")
		for instance_id in authorized_instances:
			if type(instance_id) != int:
				raise TypeError("authorized_instances parameter must be a list of instance IDs")
		self.authorized_instances = authorized_instances

		self.base_folder = "/".join(self.firmware_path.split('/')[:-1])

	def create_c_file(self):
		"""
		Create a C file based on the instance configuration
		"""

	def compile_firmware(self, clean=True, verbose=False):
		if not(self.firmware_exists()):
			self.create_c_file()
			self.update_project_conf()
			result = SkyMoteType.compile_firmware(self, make_options="", \
										 clean=clean, verbose=verbose)
			return result
		return True

	def update_project_conf(self):
		if not os.path.exists(self.base_folder+"/project-conf-template.h"):
			return
		f = open(self.base_folder+"/project-conf-template.h",'r')
		
		content = []

		for line in f:
			line=line.strip()
			if line.startswith("#define RPL_CONF_MAX_INSTANCES"):
				content.append("#define RPL_CONF_MAX_INSTANCES "+str(self.max_instances))
			elif line.startswith("#define RPL_CONF_MAX_DAG_MC_OBJECTS"):
				content.append("#define RPL_CONF_MAX_DAG_MC_OBJECTS "+str(self.max_mc_objects))
			elif line.startswith("#define RPL_CONF_MAX_AUTHORIZED_INSTANCES"):
				content.append("#define RPL_CONF_MAX_AUTHORIZED_INSTANCES "+str(len(self.authorized_instances)))
			elif line.startswith("#define RPL_CONF_DAG_MC_TYPES_ENABLED"):
				metrics = []
				for enabled_metric in self.enabled_metrics:
					metrics.append("RPL_DAG_MC_BITMAP("+str(enabled_metric)+")")
				if len(metrics) == 0:
					metrics.append("0")
				content.append("#define RPL_CONF_DAG_MC_TYPES_ENABLED ("+" | ".join(metrics)+")")
			else:
				content.append(line.strip())
		f.close()
		f2 = open(self.base_folder+'/project-conf.h','w')

		for line in content:
			f2.write(line+'\n')
		f2.close()

	def simulation_finished(self):
		SkyMoteType.simulation_finished(self)
		if os.path.exists(self.firmware_path):
			os.remove(self.firmware_path)

class SimpleUdpServerType(AbstractSkyMoteType):
	def __init__(self, instances=[], max_instances=2, max_mc_objects=1, enabled_metrics=None):
		base_folder = os.path.dirname(cooja_tools.__file__)+"/firmware/rpl-udp-server"
		firmware_path = base_folder+"/udp-server-"+hex(id(self))[2:]+".sky"
		description = "Custom Server SkyMote"
		AbstractSkyMoteType.__init__(self, firmware_path, max_instances=max_instances, \
			max_mc_objects=max_mc_objects, enabled_metrics=enabled_metrics, \
			make_target="udp-server.sky", description=description)

		self.instances = instances

	def create_c_file(self):
		f = open(self.base_folder+"/template.c",'r')
		
		content = []

		for line in f:
			content.append(line)
			if "// Variables definition" in line:
				self.variables_definition(content)

			elif "// Instances initialization" in line:
				self.instances_initialization(content)
		f.close()
		f2 = open(self.base_folder+'/udp-server.c','w')

		for line in content:
			f2.write(line)
		f2.close()

	def compile_firmware(self, clean=True, verbose=False):
		result = AbstractSkyMoteType.compile_firmware(self, clean=clean, verbose=verbose)
		if result and os.path.exists(self.base_folder+"/udp-server.sky"):
			os.rename(self.base_folder+"/udp-server.sky", self.firmware_path)
			os.remove(self.base_folder+"/udp-server.c")
			os.remove(self.base_folder+"/project-conf.h")
		return result

	def instances_initialization(self, content):
		i = 1
		for instance in self.instances:
			dodagid_str = ""
			for j in range(0,16,2):
				if j > 0:
					dodagid_str += ","
				dodagid_str += str((instance.dodag_id[j]*256+instance.dodag_id[j+1]))

			prefix_str = ""
			for j in range(0,16,2):
				if j > 0:
					prefix_str += ","
				prefix_str += str((instance.prefix[j]*256+instance.prefix[j+1]))

			content.append('instance_id'+str(i)+' = '+str(instance.id)+';\n')
			content.append('instance'+str(i)+' = rpl_alloc_instance(instance_id'+str(i)+');\n')
			content.append('uip_ip6addr(&ipaddr'+str(i)+', '+dodagid_str+');\n')
			content.append('uip_ds6_addr_add(&ipaddr'+str(i)+', 0, ADDR_MANUAL);\n')
			content.append('root_if'+str(i)+' = uip_ds6_addr_lookup(&ipaddr'+str(i)+');\n')
			content.append('of'+str(i)+' = rpl_find_of('+str(instance.of)+');\n')
			content.append('if(root_if'+str(i)+' != NULL) {\n')
			content.append('  rpl_dag_t *dag'+str(i)+';\n')
			content.append('  rpl_set_of(instance_id'+str(i)+', of'+str(i)+');\n')
			if instance.metric != None:
				content.append('  rpl_set_metric(instance'+str(i)+', '+str(instance.metric)+');\n')
			content.append('  dag'+str(i)+' = rpl_set_root(instance_id'+str(i)+',(uip_ip6addr_t *)&ipaddr'+str(i)+');\n')
			content.append('  uip_ip6addr(&ipaddr'+str(i)+', '+prefix_str+');\n')
			content.append('  rpl_set_prefix(dag'+str(i)+', &ipaddr'+str(i)+', '+str(instance.prefix_length)+');\n')
			content.append('  PRINTF("created a new RPL dag\\n");\n')
			content.append('} else {\n')
			content.append('  PRINTF("failed to create a new RPL DAG\\n");\n')
			content.append('}\n')
			i += 1

	def variables_definition(self, content):
		if len(self.instances) > 0:
			i = 1
			ip_str = "uip_ipaddr_t "
			root_if_str = "struct uip_ds6_addr "
			of_str = "rpl_of_t "
			instance_str = "rpl_instance_t "
			instance_id_str = "uint8_t "
			for instance in self.instances:
				if i > 1: ip_str += ", "
				ip_str += "ipaddr"+str(i)

				if i > 1: root_if_str += ", "
				root_if_str += "*root_if"+str(i)

				if i > 1: of_str += ", "
				of_str += "*of"+str(i)

				if i > 1: instance_str += ", "
				instance_str += "*instance"+str(i)

				if i > 1: instance_id_str += ", "
				instance_id_str += "instance_id"+str(i)
				i += 1
			ip_str += ";\n"
			root_if_str += ";\n"
			of_str += ";\n"
			instance_str += ";\n"
			instance_id_str += ";\n"
			content.append(ip_str)
			content.append(root_if_str)
			content.append(of_str)
			content.append(instance_str)
			content.append(instance_id_str)


class SimpleClientType(AbstractSkyMoteType):
	def __init__(self, max_instances=2, max_mc_objects=1, send_udp_datagram=True, start_delay=30.0, \
			send_interval=[3,5], enabled_metrics=None, ipv6_destinations=[], instance_ids=[], \
			authorized_instances=[]):
		base_folder = os.path.dirname(cooja_tools.__file__)+"/firmware/rpl-udp-client"
		firmware_path = base_folder+"/udp-client-"+hex(id(self))[2:]+".sky"
		description = "Custom Client SkyMote"
		AbstractSkyMoteType.__init__(self, firmware_path, max_instances=max_instances, \
			max_mc_objects=max_mc_objects, enabled_metrics=enabled_metrics, \
			make_target="udp-client.sky", description=description, \
			authorized_instances=authorized_instances)

		self.send_udp_datagram = send_udp_datagram
		self.start_delay = start_delay
		if type(send_interval) == int:
			send_interval = [send_interval, send_interval]
		self.send_interval = send_interval

		destinations = []
		if type(ipv6_destinations) == str:
			destinations.append(address_from_str(ipv6_destinations))
		elif type(ipv6_destinations) == list:
			for destination in ipv6_destinations:
				if type(destination) == str:
					destinations.append(address_from_str(destination))
				elif type(destination) == list:
					destinations.append(destination)
		self.ipv6_destinations = destinations

		# TODO : handle instance ID switching in time !
		ids = []
		if type(instance_ids) == int:
			ids.append(instance_ids)
		elif type(instance_ids) == list:
			for instance_id in instance_ids:
				if type(instance_id) == int:
					ids.append(instance_id)
		if len(ids) == 0:
			ids.append(0)
		self.instance_ids = ids

	def create_c_file(self):
		f = open(self.base_folder+"/template.c",'r')
		
		content = []

		for line in f:
			if line.startswith("#define SEND_MESSAGE"):
				content.append("#define SEND_MESSAGE "+str(1 if self.send_udp_datagram else 0)+"\n")
			elif line.startswith("#define START_DELAY"):
				content.append("#define START_DELAY ("+str(self.start_delay)+" * CLOCK_SECOND)\n")
			elif line.startswith("#define NB_SERVERS"):
				content.append("#define NB_SERVERS "+str(len(self.ipv6_destinations))+"\n")
			elif line.startswith("#define NB_INSTANCES"):
				content.append("#define NB_INSTANCES "+str(len(self.instance_ids))+"\n")
			elif line.startswith("#define SEND_TIME"):
				if self.send_interval[1]-self.send_interval[0] > 0:
					time1 = str(self.send_interval[0])
					time2 = str(self.send_interval[1]-self.send_interval[0])
					content.append("#define SEND_TIME ("+time1+" + random_rand() % ("+time2+" * CLOCK_SECOND))\n")
				else:
					content.append("#define SEND_TIME ("+str(self.send_interval[0])+" * CLOCK_SECOND)\n")
			elif "/* addresses initialization */" in line:
				content.append(line)
				self.variables_initialization(content)
			elif "/* addresses definition */" in line:
				content.append(line)
				self.variables_definition(content)
			elif "/* authorized instances */" in line:
				content.append(line)
				for instance_id in self.authorized_instances:
					content.append('rpl_add_authorized_instance_id('+str(instance_id)+');\n')
			else:
				content.append(line)
		f.close()
		f2 = open(self.base_folder+'/udp-client.c','w')

		for line in content:
			f2.write(line)
		f2.close()

	def compile_firmware(self, clean=True, verbose=False):
		result = AbstractSkyMoteType.compile_firmware(self,clean=clean, verbose=verbose)
		if result and os.path.exists(self.base_folder+"/udp-client.sky"):
			os.rename(self.base_folder+"/udp-client.sky", self.firmware_path)
			os.remove(self.base_folder+"/udp-client.c")
			os.remove(self.base_folder+"/project-conf.h")
		return result

	def variables_initialization(self, content):
		i = 0
		for destination in self.ipv6_destinations:
			ip_str = ""
			for j in range(0,16,2):
				if j > 0:
					ip_str += ","
				ip_str += str((destination[j]*256+destination[j+1]))

			content.append('uip_ip6addr(&server_ipaddr['+str(i)+'], '+ip_str+');\n')
			i += 1

		i = 0
		for instance_id in self.instance_ids:
			content.append('instance_ids['+str(i)+']='+str(instance_id)+';\n')
			i += 1

	def variables_definition(self, content):
		n = len(self.ipv6_destinations)
		if n > 0:
			content.append("static uip_ipaddr_t server_ipaddr["+str(n)+"];\n")

		n = len(self.instance_ids)
		if n > 0:
			content.append("static uint8_t instance_ids["+str(n)+"];\n")

class RPLInstance:
	OF_OF0 = "RPL_OCP_OF0"
	OF_MRHOF = "RPL_OCP_MRHOF"
	METRIC_ETX = 7
	METRIC_HOPCOUNT = 3
	METRIC_LATENCY = 5
	def __init__(self, instance_id=11, dodag_id="aaaa::1", prefix_length=64, of=OF_MRHOF, metric=None):
		self.id = instance_id
		self.prefix_length = prefix_length
		if type(dodag_id) == str:
			dodag_id = address_from_str(dodag_id)
		self.dodag_id = dodag_id
		self.prefix = self.dodag_id[:prefix_length/8]+([0]*(16-prefix_length/8))
		self.of = of
		self.metric = metric

def address_from_str(string, length=16):
  if type(string) == str:
    blocks = string.split(":")
    address = []

    for block in blocks:
      if len(block) != 0:
        block = block.rjust(4,"0")
        if length == 16:
          address.append(int(block[:2], 16))
        address.append(int(block[2:], 16))
      elif length == 16:
        for i in range(8-len(blocks)+1):
          address.append(0)
          address.append(0)
    return address

  else :
    return []
