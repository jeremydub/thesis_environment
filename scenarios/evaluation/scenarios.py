from cooja_tools import *
from pcap import *

def get_simulation_contiki_master():
	topology = Topology.get_tree25_topology()

	folder = "applications/rpl-udp"
		
	# Defining a sender mote using Contiki 3.0 with partial multi-instances support.
	client = SkyMoteType(folder+"/udp-client.sky")
	# Defining a sender mote using Contiki 3.0 with partial multi-instances support.
	server = SkyMoteType(folder+"/udp-server.sky")

	topology.set_mote_type(1, server)
	topology.set_mote_type(range(2,26), client)
	topology.set_mote_type(25, client)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology,seed=2234, timeout=120)

	return simulation

def get_simulation_contiki_multi():
	topology = Topology.get_tree25_topology()
		
	# Define instances (with different prefixes) that will be used in the scenario
	instance1 = RPLInstance(instance_id=10, dodag_id="fd01::1", of=RPLInstance.OF_MRHOF)
	instance2 = RPLInstance(instance_id=20, dodag_id="fd01::1", of=RPLInstance.OF_MRHOF)
	
	# Define two Contiki-based server motes, each server advertises 1 instance
	server1 = SimpleUdpServerType(instances=[instance1])
	server2 = SimpleUdpServerType(instances=[instance2])

	# Defining a Contiki-based client mote type sending no message, can join 2 instances
	client_non_sender = SimpleClientType(max_instances=2, send_udp_datagram=False)

	# Defining a client mote type that sends messages and can join 2 instances
	destinations = ['fd01::1']
	#instance_ids = [instance1.id, instance2.id]
	client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=20, ipv6_destinations=destinations, send_interval=3)

	topology.set_mote_type(1, server1)
	topology.set_mote_type(range(2,26), client_non_sender)
	topology.set_mote_type(25, client_sender)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology,seed=2234, timeout=120)

	return simulation

def get_simulation_eval1():
	topology = Topology.get_tree25_topology()
		
	# Define instances with different prefixes that will be used in the scenario
	instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF)
	instance2 = RPLInstance(instance_id=20, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF)
	
	# Define two Contiki-based server motes, each server advertises 1 instance
	server = SimpleUdpServerType(instances=[instance1, instance2])
	server1 = SimpleUdpServerType(instances=[instance1])
	server2 = SimpleUdpServerType(instances=[instance2])
	
	# Defining a Contiki-based client mote type sending no message, can join 1 instance
	client_non_sender = SimpleClientType(max_instances=2, send_udp_datagram=False)
	
	# Defining a Contiki-based client mote that sends messages and can join 2 instances.
	# The mote will switch between given instances for next hop selection.
	client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=30, ipv6_destinations='fd00::1', instance_ids=[instance1.id, instance2.id], send_interval=3)
	
	topology.set_mote_type(1, server)
	topology.set_mote_type(range(2,26), client_non_sender)
	topology.set_mote_type(25, client_sender)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology, timeout=120)

	return simulation

#simulation = get_simulation_contiki_master()
#simulation.run(log_file="output_master.log", pcap_file="master.pcap", verbose=True)
#simulation = get_simulation_contiki_multi()
#simulation.run(log_file="output_multi.log", pcap_file="multi.pcap", verbose=True)

def data_analyse1():
	pcap = PcapHandler("multi_1inst.pcap")

	frames=pcap.get_frames()

	# Filter frames that contain datagrams from a specific LL source
	datagrams = pcap.get_datagrams()
	
	# Print latency and hopcount for each unique datagram
	for datagram in datagrams:
		print "("+str(datagram.latency())+"|"+str(datagram.hopcount())+")",
	print ""

	# Trace route of 3 first datagrams of the list
	for datagram in datagrams:
		current_hop_frame = datagram
		i = 1
		while current_hop_frame != None:
			print "Hop #"+str(i), repr(current_hop_frame)
			current_hop_frame = current_hop_frame.next_hop_frame
			i += 1
		print ""

def control_analyse1(pcap_file="master_1inst.pcap"):
	pcap = PcapHandler(pcap_file)

	frames=pcap.get_frames()

	is_dio_message = lambda packet : packet.has_header(ICMPv6Header) \
					 				 and packet.get_header(ICMPv6Header).code == ICMPv6Header.RPL_CODE_DAO \
					 				 and packet.get_header(ICMPv6Header).instance_id == 10

	# All frames that contain ICMPv6 DIO messages
	dio_messages = filter(is_dio_message, frames)

	dio_messages_node = filter(lambda p: p.headers[0].source[-1] == 25, dio_messages)

	counter = 0

	for m in dio_messages:
		#print repr(m)
		#print m.get_header(ICMPv6Header).target_prefix,
		counter += m.duplicate_counter

	print len(dio_messages), counter

control_analyse1("master_1inst.pcap")
control_analyse1("multi_1inst.pcap")
control_analyse1("multi_2inst.pcap")
#exit()

#simulation = get_simulation_eval1()
#simulation.run(log_file="multi_2inst.log", pcap_file="multi_2inst.pcap", verbose=True)
#simulation = get_simulation_contiki_master()
#simulation.run(log_file="master_1inst.log", pcap_file="master_1inst.pcap", verbose=True)
#simulation = get_simulation_contiki_multi()
#simulation.run(log_file="multi_1inst.log", pcap_file="multi_1inst.pcap", verbose=True)

#simulation.run_with_cooja()