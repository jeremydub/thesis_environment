from cooja_tools import *
from pcap import *

def get_simulation_scenario1():
	# Load topology of 5 motes : 
	#    (1)       (2)
	#     \        /
	#     (3)    (4)
	#      \     /
	#        (5)
	#
	# Mote types are not preseved while imported because they
	# will be set later.
	topology = Topology.from_csc('topology1.csc')	
	
	# Define instances (with different prefixes) that will be used in the scenario
	instance1 = RPLInstance(instance_id=10, dodag_id="aaaa::1", of=RPLInstance.OF_OF0)
	instance2 = RPLInstance(instance_id=20, dodag_id="bbbb::1", of=RPLInstance.OF_MRHOF)
	
	# Define two Contiki-based server motes, each server advertises 1 instance
	server1 = SimpleUdpServerType(instances=[instance1])
	server2 = SimpleUdpServerType(instances=[instance2])
	
	# Defining a Contiki-based client mote type sending no message, can join 1 instance.
	client_non_sender = SimpleClientType(max_instances=1, send_udp_datagram=False)
	
	# Defining a Contiki-based client mote that sends messages and can join 2 instances.
	destinations = ['aaaa::1', 'bbbb::1']
	client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=60, ipv6_destinations=destinations, send_interval=10)
	
	# Set mote types
	topology.set_mote_type([3,4], client_non_sender)
	topology.set_mote_type(1, server1)
	topology.set_mote_type(2, server2)
	topology.set_mote_type(5, client_sender)
	
	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology, timeout=300, title="Scenario 1")

	return simulation

def get_simulation_scenario2():
	# Load topology of 5 motes : 
	#    (1)       (2)
	#     \        /
	#     (3)    (4)
	#      \     /
	#        (5)
	#
	# Mote types are not preseved while imported because they
	# will be set later.
	topology = Topology.from_csc('topology1.csc')
	
	# Define instances with different prefixes that will be used in the scenario
	instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_OF0)
	instance2 = RPLInstance(instance_id=20, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF)
	
	# Define two Contiki-based server motes, each server advertises 1 instance
	server1 = SimpleUdpServerType(instances=[instance1])
	server2 = SimpleUdpServerType(instances=[instance2])
	
	# Defining a Contiki-based client mote type sending no message, can join 1 instance
	client_non_sender = SimpleClientType(max_instances=1, send_udp_datagram=False)
	
	# Defining a Contiki-based client mote that sends messages and can join 2 instances.
	# The mote will switch between given instances for next hop selection.
	client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=60, ipv6_destinations='fd00::1', instance_ids=[instance1.id, instance2.id], send_interval=10)
	
	# Set mote types
	topology.set_mote_type([3,4], client_non_sender)
	topology.set_mote_type(1, server1)
	topology.set_mote_type(2, server2)
	topology.set_mote_type(5, client_sender)
	
	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology, timeout=300, title="Scenario 2")

	return simulation


def get_simulation_scenario3():
	# Load topology of 5 motes : 
	#         (1)
	#       /    \
	#     (2)    (3)
	#     /       \
	#   (4)       (5)
	
	#
	# Mote types are not preseved while imported because they
	# will be set later.
	topology = Topology.from_csc('topology2.csc', preserve_mote_types=False)	

	# Define a Contiki-based server mote that advertises 2 instances
	instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_OF0)
	instance2 = RPLInstance(instance_id=20, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF)
	server = SimpleUdpServerType(instances=[instance1, instance2])

	# Defining a Contiki-based client mote type sending no message, can join instance 10
	client2 = SimpleClientType(max_instances=1, send_udp_datagram=False, authorized_instances=[10])
	# Defining a Contiki-based client mote type sending no message, can join instance 20
	client3 = SimpleClientType(max_instances=1, send_udp_datagram=False, authorized_instances=[20])

	# Defining a client mote type that sends messages and can join 1 instance
	destinations = ['fd00::1']
	client_sender = SimpleClientType(max_instances=1, send_udp_datagram=True, start_delay=30, ipv6_destinations=destinations, send_interval=10)

	# Set mote types
	topology.set_mote_type(1, server)
	topology.set_mote_type(2, client2)
	topology.set_mote_type(3, client3)
	topology.set_mote_type([4,5], client_sender)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology, timeout=300, title="Scenario 4")

	return simulation

def get_simulation_scenario4():
	# Load topology of 5 motes : 
	#         (1)
	#       /    \
	#     (2)    (3)
	#      \     /
	#        (4)
	
	#
	# Mote types are not preseved while imported because they
	# will be set later.
	topology = Topology.from_csc('topology3.csc', preserve_mote_types=False)	

	# Define a Contiki-based server mote that advertises 2 instances
	instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_OF0)
	instance2 = RPLInstance(instance_id=20, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF)
	server = SimpleUdpServerType(instances=[instance1, instance2])

	# Defining a Contiki-based client mote type sending no message, can join 2 instances
	client_non_sender = SimpleClientType(max_instances=2, send_udp_datagram=False)

	# Defining a client mote type that sends messages and can join 2 instances
	destinations = ['fd00::1']
	instance_ids = [instance1.id, instance2.id]
	client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=30, ipv6_destinations=destinations, instance_ids=instance_ids, send_interval=10)
	# Set mote types
	topology.set_mote_type(1, server)
	topology.set_mote_type([2,3], client_non_sender)
	topology.set_mote_type(4, client_sender)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology, timeout=300, title="Scenario 4")

	return simulation

def get_simulation_scenario4_old():
	# Load topology of 5 motes : 
	#         (1)
	#       /    \
	#     (2)    (3)
	#      \     /
	#        (4)
	
	#
	# Mote types are not preseved while imported because they
	# will be set later.
	topology = Topology.from_csc('topology3.csc', preserve_mote_types=False)	

	# Define a Contiki-based server mote that advertises 2 instances
	instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_OF0)
	instance2 = RPLInstance(instance_id=20, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF)
	server = SimpleUdpServerType(instances=[instance1, instance2])

	# Defining a client mote type that does not send messages and can join instance 10
	client2 = SimpleClientType(max_instances=1, send_udp_datagram=False, authorized_instances=[10])
	# Defining a client mote type that does not send messages and can join instance 20
	client3 = SimpleClientType(max_instances=1, send_udp_datagram=False, authorized_instances=[20])

	# Defining a client mote type that sends messages and can join 2 instances
	destinations = ['fd00::1']
	instance_ids = [instance1.id, instance2.id]
	client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=30, ipv6_destinations=destinations, instance_ids=instance_ids, send_interval=10)
	# Set mote types
	topology.set_mote_type(1, server)
	topology.set_mote_type(2, client2)
	topology.set_mote_type(3, client3)
	topology.set_mote_type(4, client_sender)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology, timeout=300, title="Scenario 4")

	return simulation

def get_simulation_scenario5():
	folder = "/home/user/jeremy/old/programs/rpl-multi-instances"
		
	# Define instances (with different prefixes) that will be used in the scenario
	instance1 = RPLInstance(instance_id=10, dodag_id="aaaa::1", of=RPLInstance.OF_MRHOF)
	instance2 = RPLInstance(instance_id=20, dodag_id="bbbb::1", of=RPLInstance.OF_MRHOF)
	
	# Define two Contiki-based server motes, each server advertises 1 instance
	server1 = SimpleUdpServerType(instances=[instance1])
	server2 = SimpleUdpServerType(instances=[instance2])

	# Defining a sender mote using Contiki 3.0 with partial multi-instances support.
	client = SkyMoteType(folder+"/udp-client.sky")

	# create topology of 3 motes : 
	#    (1)     (2)
	#     \     /
	#       (3)
	#
	motes = []
	motes.append(SkyMote(1,-65,-0, server1))
	motes.append(SkyMote(2,65,0, server2))
	motes.append(SkyMote(3,0,60, client))

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(Topology(motes),seed=1234)

	return simulation

def get_eval1_simulation():
	topology = Topology.get_tree25_topology()

	folder = "/home/user/jeremy/old/programs/rpl-udp"
		
	# Define instances (with different prefixes) that will be used in the scenario
	instance1 = RPLInstance(instance_id=10, dodag_id="fd01::1", of=RPLInstance.OF_MRHOF)
	instance2 = RPLInstance(instance_id=20, dodag_id="fd01::1", of=RPLInstance.OF_MRHOF)
	
	# Define two Contiki-based server motes, each server advertises 1 instance
	server1 = SimpleUdpServerType(instances=[instance1])
	server2 = SimpleUdpServerType(instances=[instance2])

	# Defining a sender mote using Contiki 3.0 with partial multi-instances support.
	client = SkyMoteType(folder+"/udp-client.sky")
	# Defining a sender mote using Contiki 3.0 with partial multi-instances support.
	server = SkyMoteType(folder+"/udp-server.sky")

	# Defining a Contiki-based client mote type sending no message, can join 2 instances
	client_non_sender = SimpleClientType(max_instances=2, send_udp_datagram=False)

	# Defining a client mote type that sends messages and can join 2 instances
	destinations = ['fd01::1']
	#instance_ids = [instance1.id, instance2.id]
	client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=30, ipv6_destinations=destinations, send_interval=10)

	topology.set_mote_type(1, server)
	topology.set_mote_type(range(2,26), client)
	topology.set_mote_type(25, client)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology,seed=1234)

	return simulation


def compute_struct_sizeof():
	folder = "/home/user/jeremy/old/programs/rpl-sizeof"

	# Defining a sender mote using Contiki 3.0 with partial multi-instances support.
	mote_type = SkyMoteType(folder+"/rpl-sizeof.sky")
	mote_type.compile_firmware(clean=True)

	motes = [SkyMote(1,0,0, mote_type)]
	topology = Topology(motes)

	# Create a simulation with default RX/TX success ratio, seed and timeout values
	simulation = CoojaSimulation(topology, timeout=5)

	simulation.run(log_file="sizeof.log")
	f=open(simulation.get_log_filepath())
	for line in f:
		print line.strip()

#compute_struct_sizeof()

#simulation = get_simulation_scenario4()
simulation = get_eval1_simulation()
simulation.run_with_cooja()