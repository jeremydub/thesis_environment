from cooja_tools import *
from pcap import *

db = SimulationsDatabase()

def simulation_finished(simulation):
	if simulation.has_suceed():
		db.add_simulation(simulation)

def test():
	topology = Topology.get_tree25_topology()	

	# Defining mote type for server
	instance1 = RPLInstance(instance_id=10, dodag_id="aaaa::1", \
							of=RPLInstance.OF_MRHOF, metric=None)
	instance2 = RPLInstance(instance_id=20, dodag_id="bbbb::1", of=RPLInstance.OF_OF0, metric=None)
	
	# Defining DODAG roots for each scenario
	server_instance1 = SimpleUdpServerType(instances=[instance1])
	server_instance2 = SimpleUdpServerType(instances=[instance2])
	server_both_instances = SimpleUdpServerType(instances=[instance1, instance2])

	# Defining a client mote type that does not send messages
	client_non_sender = SimpleClientType(max_instances=2, send_udp_datagram=False)
	
	# Defining a client mote type that send messages
	destinations = ['aaaa::1', 'bbbb::1']

	# Defining non-root motes for each scenario
	client_sender_instance1 = SimpleClientType(max_instances=2, send_udp_datagram=True, \
			start_delay=60, ipv6_destinations=destinations[:1], send_interval=10)
	client_sender_instance2 = SimpleClientType(max_instances=2, send_udp_datagram=True, \
			start_delay=60, ipv6_destinations=destinations[1:], send_interval=10)
	client_sender_both_instances = SimpleClientType(max_instances=2, send_udp_datagram=True, \
			start_delay=60, ipv6_destinations=destinations, send_interval=10)

	simulations_worker = SimulationWorker(callback=simulation_finished)

	rx_list = [0.2,0.4,0.6,0.8,1.0]

	for rx in rx_list:
		for scenario in range(1,4):
			if scenario == 1:
				topology.set_mote_type(range(1,26), client_non_sender)
				topology.set_mote_type(1,server_instance1)
				topology.set_mote_type(25, client_sender_instance1)
			elif scenario == 2:
				topology.set_mote_type(range(1,26), client_non_sender)
				topology.set_mote_type(1,server_instance2)
				topology.set_mote_type(25, client_sender_instance2)
			elif scenario == 3:
				topology.set_mote_type(range(1,26), client_non_sender)
				topology.set_mote_type(1,server_both_instances)
				topology.set_mote_type(25, client_sender_both_instances)

			# Create a simulation with default RX/TX success ratio, seed and timeout values
			title = "Simulation scenario "+str(scenario)
			simulation = CoojaSimulation(topology, timeout=1800, success_ratio_rx=rx, title=title, debug_info={'id':22, 'scenario':scenario})

			simulations_worker.add_simulation(simulation)

	simulations_worker.run()
	db.commit_changes()

def test2():
	topology = Topology.get_tree25_topology()	

	# Defining mote type for server
	instance1 = RPLInstance(instance_id=10, dodag_id="aaaa::1", \
							of=RPLInstance.OF_MRHOF, metric=None)
	instance2 = RPLInstance(instance_id=20, dodag_id="bbbb::1", of=RPLInstance.OF_OF0, metric=None)
	
	# Defining DODAG roots for each scenario
	server_instance1 = SimpleUdpServerType(instances=[instance1])
	server_instance2 = SimpleUdpServerType(instances=[instance2])
	server_both_instances = SimpleUdpServerType(instances=[instance1, instance2])

	# Defining a client mote type that does not send messages
	client_non_sender = SimpleClientType(max_instances=2, send_udp_datagram=False)
	
	# Defining a client mote type that send messages
	destinations = ['aaaa::1', 'bbbb::1']

	simulations_worker = SimulationWorker(callback=simulation_finished)

	rx_list = [0.2,0.4,0.6,0.8,1.0]

	for rx in rx_list:
		for scenario in range(1,4):
			if scenario == 1:
				topology.set_mote_type(range(1,26), client_non_sender)
				topology.set_mote_type(1,server_instance1)
			elif scenario == 2:
				topology.set_mote_type(range(1,26), client_non_sender)
				topology.set_mote_type(1,server_instance2)
			elif scenario == 3:
				topology.set_mote_type(range(1,26), client_non_sender)
				topology.set_mote_type(1,server_both_instances)

			# Create a simulation with default RX/TX success ratio, seed and timeout values
			title = "Simulation scenario "+str(scenario)+", no sender"
			simulation = CoojaSimulation(topology, timeout=1800, success_ratio_rx=rx, title=title, debug_info={'id':3, 'send':False})

			simulations_worker.add_simulation(simulation)

	simulations_worker.run()
	db.commit_changes()

def analyse3():
	pcap = PcapHandler("../data/simulation1.pcap")
	frames=pcap.get_frames()

	# Filter frames that contain datagrams from a specific LL source
	datagrams = pcap.get_datagrams(ll_source="00:12:74:19:00:19:19:19")

	plotter = GraphPlotter()

	# group packets by RPL instance ID
	dest_to_packets = pcap.group_frames_by_attribute(datagrams, lambda p : p.get_header(IPv6Header).source[0])

	for destination in dest_to_packets:

		messages = dest_to_packets[destination]

		# Creating coordonates
		x_values = []
		y_values = []
		for message in messages:
			latency = message.latency()
			if latency != Packet.INFINITE_TIME:
				x_values.append(message.timestamp)
				y_values.append(latency)

		# Plotting functions
		plot = Plot("Messages ", True, "Time","Latency",identifier="destination_"+str(destination)+"__time_vs_latency")
		plot.add_subplot(x_values,y_values)
		plot.add_subplot(x_values,y_values, draw_symbol="+")
		plotter.plot(plot, "simulation1_lossy")

def analyse4():

	plotter = GraphPlotter()

	topology = Topology.get_tree25_topology()
	simulations = db.query(topology=topology, debug_info={'id':22})
	#simulations = db.query(topology=topology, success_ratio_rx=1.0, debug_info={'send':False})
	id_to_pcap = {}

	extract_node_id = lambda p : p.get_header(IPv6Header).source[-1]
	# Frame filter functions
	is_dio_message = lambda packet : packet.has_header(ICMPv6Header) \
					 				 and packet.get_header(ICMPv6Header).code == ICMPv6Header.RPL_CODE_DIO
	is_udp_datagram = lambda packet : packet.has_header(UDPHeader)

	simulations = simulations

	scenario_to_simulations = {}
	for simulation in simulations:
		scenario = simulation.debug_info['scenario']
		if not scenario_to_simulations.has_key(scenario):
			scenario_to_simulations[scenario] = []
		scenario_to_simulations[scenario].append(simulation)

	print "Parsing Pcap files ..."
	for simulation in simulations:
		pcap = PcapHandler(simulation.get_pcap_filepath())
		id_to_pcap[simulation.id] = pcap

	if False:
		print "Exporting plot(s) ..."
		plot = Plot("Messages", "Time","Latency",identifier="time_vs_latency")
		for simulation in simulations:
			handler = id_to_pcap[simulation.id]
			packets = handler.get_datagrams(ll_source="00:12:74:19:00:19:19:19")
			packets = filter(lambda x: x.latency() != Packet.INFINITE_TIME, packets)
			(data_x, data_y) = DataGraph.two_dimensional(packets, lambda p : p.timestamp, lambda p : p.latency())
			plot.add_subplot(data_x, data_y, "Scenario "+str(simulation.debug_info['scenario']))
		plotter.plot(plot, folder="2")
	
	# DIO
	if False:
		for mote in topology:
			plot_by_instance_id={}
			for simulation in simulations:
				handler = id_to_pcap[simulation.id]
				dio_messages = filter(lambda x : is_dio_message(x) and extract_node_id(x)==mote.id, handler.get_frames())

				messages_by_instance_id = handler.group_frames_by_attribute(dio_messages, lambda x : x.get_header(ICMPv6Header).instance_id)

				for instance_id in messages_by_instance_id:
					if not plot_by_instance_id.has_key(instance_id):
						identifier = "instance"+str(instance_id)+"__node"+str(mote.id)+"__time_vs_rank"
						plot = Plot("Rank (Node "+str(mote.id)+")", "Time","Rank",identifier=identifier)
						plot_by_instance_id[instance_id] = plot

					packets = messages_by_instance_id[instance_id]

					(data_x, data_y) = DataGraph.two_dimensional(packets, lambda p : p.timestamp, lambda p : p.get_header(ICMPv6Header).rank)
					plot_by_instance_id[instance_id].add_subplot(data_x, data_y, "Scenario "+str(simulation.debug_info['scenario']))
			

			for instance_id in plot_by_instance_id:
				plotter.plot(plot_by_instance_id[instance_id], folder="2")

	if False:
		for scenario in scenario_to_simulations:
			identifier = "scenario"+str(scenario)+"__cdf_dio"
			plot_pdr = Plot("CDF DIO", "Time","DIO",identifier=identifier)

			instance_to_points = {}

			scenario_simulations = scenario_to_simulations[scenario]
			scenario_simulations.sort(key=lambda x: x.success_ratio_rx)

			for simulation in scenario_simulations:
				print simulation
				handler = id_to_pcap[simulation.id]
				dio_messages = filter(lambda x : is_dio_message(x) and extract_node_id(x)==mote.id, handler.get_frames())

				messages_by_instance_id = handler.group_frames_by_attribute(dio_messages, lambda x : x.get_header(ICMPv6Header).instance_id)

				for instance_id in messages_by_instance_id:
					if not instance_to_points.has_key(instance_id):
						instance_to_points[instance_id] = [[],[], [], []]
					x = simulation.success_ratio_rx
					y = get_avg_latency(instance_to_datagrams[instance_id])
					y2 = get_avg_hopcount(instance_to_datagrams[instance_id])
					y3 = get_pdr(instance_to_datagrams[instance_id])
					instance_to_points[instance_id][0].append(x)
					instance_to_points[instance_id][1].append(y)

			for instance_id in instance_to_points:
				plot_latency.add_subplot(instance_to_points[instance][0], instance_to_points[instance][1], "instance "+hex(instance))
			plotter.plot(plot_pdr, y_limit=[0,1], folder="2")

	if True:

		for scenario in scenario_to_simulations:
			print ""
			print ""
			print "scenario : ", scenario

			identifier = "scenario"+str(scenario)+"__rx_vs_avg_hopcount"
			plot_hopcount = Plot("Avg Hopcount", "Rx","Avg Hopcount",identifier=identifier)
			identifier = "scenario"+str(scenario)+"__rx_vs_avg_latency"
			plot_latency = Plot("Avg Latency", "Rx","Avg Latency",identifier=identifier)
			identifier = "scenario"+str(scenario)+"__rx_vs_pdr"
			plot_pdr = Plot("PDR", "Rx","PDR",identifier=identifier)

			instance_to_points = {}

			scenario_simulations = scenario_to_simulations[scenario]
			scenario_simulations.sort(key=lambda x: x.success_ratio_rx)

			for simulation in scenario_simulations:
				print simulation
				handler = id_to_pcap[simulation.id]
				packets = handler.get_datagrams(ll_source="00:12:74:19:00:19:19:19")

				instance_to_datagrams = handler.group_frames_by_attribute(packets, lambda x : x.get_header(IPv6Header).destination[0])

				for instance in instance_to_datagrams:
					if not instance_to_points.has_key(instance):
						instance_to_points[instance] = [[],[], [], []]
					x = simulation.success_ratio_rx
					y = get_avg_latency(instance_to_datagrams[instance])
					y2 = get_avg_hopcount(instance_to_datagrams[instance])
					y3 = get_pdr(instance_to_datagrams[instance])
					instance_to_points[instance][0].append(x)
					instance_to_points[instance][1].append(y)
					instance_to_points[instance][2].append(y2)
					instance_to_points[instance][3].append(y3)

			for instance in instance_to_points:
				plot_latency.add_subplot(instance_to_points[instance][0], instance_to_points[instance][1], "instance "+hex(instance))
				plot_hopcount.add_subplot(instance_to_points[instance][0], instance_to_points[instance][2], "instance "+hex(instance))
				plot_pdr.add_subplot(instance_to_points[instance][0], instance_to_points[instance][3], "instance "+hex(instance))
			plotter.plot(plot_hopcount, y_limit=[0,5], folder="22")
			plotter.plot(plot_latency, y_limit =[0, 5], folder="22")
			plotter.plot(plot_pdr, y_limit=[0,1], folder="22")




def get_avg_hopcount(datagrams):
	datagrams = filter(lambda x: x.latency() != Packet.INFINITE_TIME, datagrams)
	
	if len(datagrams) == 0:
		return 0

	sum_hopcount = reduce(lambda x,y: x+y, map(lambda x: x.hopcount(), datagrams))

	return float(sum_hopcount) / len(datagrams)

def get_avg_latency(datagrams):
	datagrams = filter(lambda x: x.latency() != Packet.INFINITE_TIME, datagrams)
	
	if len(datagrams) == 0:
		return 0

	sum_latency = reduce(lambda x,y: x+y, map(lambda x: x.latency(), datagrams))

	return float(sum_latency) / len(datagrams)

def get_pdr(datagrams):
	packet_sent = len(datagrams)
	datagrams = filter(lambda x: x.latency() != Packet.INFINITE_TIME, datagrams)
	packets_received = len(datagrams)
	
	if len(datagrams) == 0:
		return 0.0

	return float(packets_received) / packet_sent


def t1():
	plotter = GraphPlotter()

	n=15
	if not os.path.exists("simulation.pcap"):
		# Defining mote type for server
		instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF, metric=None)
		server = SimpleUdpServerType(instances=[instance1])

		# Defining a client mote type that does not send messages and can join 2 instances
		client_non_sender = SimpleClientType(max_instances=1, send_udp_datagram=False)
		
		# Defining a client mote type that sends messages and can join 2 instances
		destinations = ['fd00::1']
		client_sender = SimpleClientType(max_instances=1, send_udp_datagram=True, start_delay=60, ipv6_destinations=destinations, send_interval=5)

		motes=[]
		current_mote_id=1
		for i in range(n):
			x = 0
			y = i*30

			if current_mote_id == 1:
				mote_type = server 
			else:
				mote_type = client_sender

			new_mote = SkyMote(current_mote_id,x,y, mote_type)
			motes.append(new_mote)
			current_mote_id += 1

		topology = Topology(motes, transmitting_range=50, interference_range=50)
		topology.set_mote_type(1, server)
		topology.set_mote_type(n, client_sender)

		# Create a simulation
		simulation = CoojaSimulation(topology, seed=15468, timeout=120)

		# Run the simulation and store the log and radio data in specific files
		#simulation.run_with_cooja()
		simulation.run(log_file="simulation.log",pcap_file="simulation.pcap", verbose=True)

	# Pcap File handler
	handler = PcapHandler("simulation.pcap")
	
	# Get radio packets as objects
	frames = handler.get_frames()

	if False:
		datagrams = handler.get_datagrams(ll_source="00:12:74:05:00:05:05:05")

		for d in datagrams:
			print repr(d)

		print len(datagrams), "datagrams sent from node 5"

		for i in range(1,n+1):
			hex_id = hex(i)[2:]
			ll_addr = "00:12:74:"+hex_id.zfill(2)+":00:"+hex_id.zfill(2)+":"+hex_id.zfill(2)+":"+hex_id.zfill(2)
			nb_packet_forwarded = handler.get_udp_traffic_load(ll_addr)
			print "node", i, ":", nb_packet_forwarded

			datagrams = handler.get_datagrams(ll_source=ll_addr)

			for d in datagrams:
				print repr(d), d.latency(), d.hopcount()
			print "\n"

	if True:
		is_dio_message = lambda packet : packet.has_header(ICMPv6Header) \
			and packet.get_header(ICMPv6Header).code == ICMPv6Header.RPL_CODE_DIO
		dio_messages = filter(lambda x : is_dio_message(x), \
			handler.get_frames())

		dio_messages = filter(lambda x : x.get_header(IEEE802154Header).source[-1] == 5, dio_messages)

		instance_id_to_dios = handler.group_frames_by_attribute(dio_messages, lambda x : x.get_header(ICMPv6Header).instance_id)

		for instance_id in instance_id_to_dios:
			dios = instance_id_to_dios[instance_id]
			dios.sort(key=lambda p: p.timestamp)

			i = 0
			data_x = []
			data_y = []

			for dio in dios:
				data_x.append(dio.timestamp)
				data_y.append(i)
				i += 1

			print data_x
			print ""
			print data_y

			plot = Plot("CDF DIO ", "Time","Pourcent",identifier="cdf_dio_instance_"+str(instance_id))
			plot.add_subplot(data_x,data_y)
			#plot.add_subplot(data_x,data_y, draw_symbol="+")
			plotter.plot(plot)

def t2():
	if True:
		if False:
			# Defining mote type for server
			instance1 = RPLInstance(instance_id=10, dodag_id="aaaa::1", of=RPLInstance.OF_OF0, metric=None)
			instance2 = RPLInstance(instance_id=20, dodag_id="bbbb::1", of=RPLInstance.OF_MRHOF, metric=None)
			server = SimpleUdpServerType(instances=[instance1, instance2])

			# Defining a client mote type that does not send messages
			client_non_sender = SimpleClientType(max_instances=2, send_udp_datagram=False)
			
			# Defining a client mote type that send messages
			destinations = ['aaaa::1']
			instance_ids = [instance1.id, instance2.id]
			client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=20, \
				ipv6_destinations=destinations, instance_ids=instance_ids, send_interval=10)

			n=3
			motes=[]
			current_mote_id=1
			for i in range(n):
				x = 0
				y = i*30

				if current_mote_id == 1:
					mote_type = server 
				else:
					mote_type = client_non_sender

				new_mote = SkyMote(current_mote_id,x,y, mote_type)
				motes.append(new_mote)
				current_mote_id += 1

			topology = Topology(motes, transmitting_range=50, interference_range=50)
			topology.set_mote_type(1, server)
			topology.set_mote_type(n, client_sender)

		if False:
			# Defining mote type for server
			instance1 = RPLInstance(instance_id=10, dodag_id="aaaa::1", of=RPLInstance.OF_OF0, metric=None)
			instance2 = RPLInstance(instance_id=20, dodag_id="bbbb::1", of=RPLInstance.OF_MRHOF, metric=None)
			server = SimpleUdpServerType(instances=[instance1, instance2])

			# Defining a client mote type that does not send messages
			client_non_sender1 = SimpleClientType(max_instances=1, send_udp_datagram=False, \
				authorized_instances=[instance1.id])
			client_non_sender2 = SimpleClientType(max_instances=1, send_udp_datagram=False, \
				authorized_instances=[instance2.id])
			
			# Defining a client mote type that send messages
			destinations = ['bbbb::1','aaaa::1']
			instance_ids = [instance1.id, instance2.id]
			client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=20, \
				ipv6_destinations=destinations, instance_ids=instance_ids, send_interval=10)

			motes=[]
			current_mote_id=1
			x,y=(0,0)
			new_mote = SkyMote(current_mote_id,x,y, server)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(-30,27)
			new_mote = SkyMote(current_mote_id,x,y, client_non_sender1)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(30,27)
			new_mote = SkyMote(current_mote_id,x,y, client_non_sender2)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(0,60)
			new_mote = SkyMote(current_mote_id,x,y, client_sender)
			motes.append(new_mote)

			n = current_mote_id

			topology = Topology(motes, transmitting_range=50, interference_range=50)

		if False:
			# Defining mote type for server
			instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_OF0, metric=None)
			instance2 = RPLInstance(instance_id=20, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF, metric=None)
			server = SimpleUdpServerType(instances=[instance1, instance2])

			# Defining a client mote type that does not send messages
			client_non_sender1 = SimpleClientType(max_instances=1, send_udp_datagram=False, \
				authorized_instances=[instance1.id])
			client_non_sender2 = SimpleClientType(max_instances=1, send_udp_datagram=False, \
				authorized_instances=[instance2.id])
			
			# Defining a client mote type that send messages
			destinations = ['fd00::1']
			instance_ids = [instance1.id, instance2.id]
			client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=20, \
				ipv6_destinations=destinations, instance_ids=instance_ids, send_interval=10)

			motes=[]
			current_mote_id=1
			x,y=(0,0)
			new_mote = SkyMote(current_mote_id,x,y, server)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(-30,27)
			new_mote = SkyMote(current_mote_id,x,y, client_non_sender1)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(30,27)
			new_mote = SkyMote(current_mote_id,x,y, client_non_sender2)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(0,60)
			new_mote = SkyMote(current_mote_id,x,y, client_sender)
			motes.append(new_mote)

			n = current_mote_id

			topology = Topology(motes, transmitting_range=50, interference_range=50)

		if False:
			# Defining mote type for server
			instance1 = RPLInstance(instance_id=10, dodag_id="fd00::1", of=RPLInstance.OF_OF0, metric=None)
			instance2 = RPLInstance(instance_id=20, dodag_id="fd00::1", of=RPLInstance.OF_MRHOF, metric=None)
			server1 = SimpleUdpServerType(instances=[instance1])
			server2 = SimpleUdpServerType(instances=[instance2])

			# Defining a client mote type that does not send messages
			client_non_sender = SimpleClientType(max_instances=1, send_udp_datagram=False)
			
			# Defining a client mote type that send messages
			destinations = ['fd00::1']
			instance_ids = [instance1.id, instance2.id]
			client_sender = SimpleClientType(max_instances=2, send_udp_datagram=True, start_delay=20, \
				ipv6_destinations=destinations, instance_ids=instance_ids, send_interval=10)

			motes=[]
			current_mote_id=1
			x,y=(-30,0)
			new_mote = SkyMote(current_mote_id,x,y, server1)
			motes.append(new_mote)

			current_mote_id += 1
			x,y=(30,0)
			new_mote = SkyMote(current_mote_id,x,y, server2)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(-30,27)
			new_mote = SkyMote(current_mote_id,x,y, client_non_sender)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(30,27)
			new_mote = SkyMote(current_mote_id,x,y, client_non_sender)
			motes.append(new_mote)
			
			current_mote_id += 1
			x,y=(0,60)
			new_mote = SkyMote(current_mote_id,x,y, client_sender)
			motes.append(new_mote)

			n = current_mote_id

			topology = Topology(motes, transmitting_range=50, interference_range=50)

		# Create a simulation with default RX/TX success ratio, seed and timeout values
		simulation = CoojaSimulation(topology, timeout=60)

		# export the simulation in a folder
		simulation.run_with_cooja()
		exit()
		#simulation.export()
		#simulation.run(log_file="simulation.log",pcap_file="simulation.pcap", verbose=True)

	handler = PcapHandler("simulation.pcap")

	for i in range(1,n+1):
		hex_id = hex(i)[2:]
		ll_addr = "00:12:74:"+hex_id.zfill(2)+":00:"+hex_id.zfill(2)+":"+hex_id.zfill(2)+":"+hex_id.zfill(2)
		print "node", i

		datagrams = handler.get_datagrams(ll_source=ll_addr)

		for d in datagrams:
			print repr(d), hex(d.get_header(IPv6Header).source[0]), hex(d.get_header(IPv6Header).destination[0]), d.latency(), d.hopcount()
		print "\n"


t2()

exit()
test()
exit()
analyse4()