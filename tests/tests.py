from cooja_tools import *
from pcap import *

db = SimulationsDatabase()

filepath="rpl-udp/rpl-udp.csc"

def simulation_finished(simulation):
	if simulation.has_suceed():
		db.add_simulation(simulation)

def f1():

	# Create a simulation object from an existing .csc file.
	simulation = CoojaSimulation.from_csc(filepath)
	simulation.timeout = 5
	simulation.success_ratio_rx = 0.4

	# Run the simulation and store the log and radio data in files
	simulation.run(pcap_file="example3.pcap", log_file="example3.log", verbose=True)
	#simulation.run_with_cooja()

	db.add_simulation(simulation)
	db.commit_changes()

def test():

	simulations_worker = SimulationWorker(callback=simulation_finished)

	rx_list = [0.2,0.4,0.5,0.6,0.7,0.8,0.9,1.0]

	for rx in rx_list:
		# Create a simulation with default RX/TX success ratio, seed and timeout values
		simulation = CoojaSimulation.from_csc(filepath)
		simulation.timeout = 300
		simulation.success_ratio_rx = rx
		simulation.debug_info={'id':1}

		name = "simulation1_"+str(rx)
		simulations_worker.add_simulation(simulation,{'pcap_file':name+".pcap", 'log_file':name+".log"})

	simulations_worker.run()
	db.commit_changes()

def f2():
	pcap = PcapHandler("example3.pcap")
	frames=pcap.get_frames()

	# Filter frames that contain datagrams from a specific LL source
	datagrams = pcap.get_datagrams()
	
	datagrams.sort(key=lambda datagram: datagram.latency(), reverse=True)

	# Print latency and hopcount for each unique datagram
	for datagram in datagrams[:10]:
		print repr(datagram)
		print "Latency:", datagram.latency(), ", Hopcount:", datagram.hopcount() 
		print ""

	# Trace route of 3 first datagrams of the list
	for datagram in datagrams[:3]:
		current_hop_frame = datagram
		i = 1
		while current_hop_frame != None:
			print "Hop #"+str(i), repr(current_hop_frame)
			current_hop_frame = current_hop_frame.next_hop_frame
			i += 1
		print ""

def analyse():

	plotter = GraphPlotter()
	simulations = db.query(debug_info={'id':1})
	id_to_pcap = {}

	scenario = 0
	identifier = "scenario"+str(scenario)+"__rx_vs_avg_hopcount"
	plot_hopcount = Plot("Avg Hopcount", "Rx","Avg Hopcount",identifier=identifier)
	identifier = "scenario"+str(scenario)+"__rx_vs_avg_latency"
	plot_latency = Plot("Avg Latency", "Rx","Avg Latency",identifier=identifier)
	identifier = "scenario"+str(scenario)+"__rx_vs_pdr"
	plot_pdr = Plot("PDR", "Rx","PDR",identifier=identifier)

	instance_to_points = {}

	simulations.sort(key=lambda x: x.success_ratio_rx)

	print "Parsing Pcap files ..."
	for simulation in simulations:
		pcap = PcapHandler(simulation.get_pcap_filepath())
		id_to_pcap[simulation.id] = pcap

	for simulation in simulations:
		print simulation
		handler = id_to_pcap[simulation.id]
		packets = handler.get_datagrams(ll_source="00:12:74:02:00:02:02:02")

		instance_to_datagrams = handler.group_frames_by_attribute(packets, lambda x : x.get_header(IPv6Header).destination[0])

		for instance in instance_to_datagrams:
			if not instance_to_points.has_key(instance):
				instance_to_points[instance] = [[],[], [], []]
			x = simulation.success_ratio_rx
			y = get_avg_latency(instance_to_datagrams[instance])
			y2 = get_avg_hopcount(instance_to_datagrams[instance])
			y3 = get_pdr(instance_to_datagrams[instance])
			instance_to_points[instance][0].append(x)
			instance_to_points[instance][1].append(y)
			instance_to_points[instance][2].append(y2)
			instance_to_points[instance][3].append(y3)

	for instance in instance_to_points:
		plot_latency.add_subplot(instance_to_points[instance][0], instance_to_points[instance][1], "instance "+hex(instance))
		plot_hopcount.add_subplot(instance_to_points[instance][0], instance_to_points[instance][2], "instance "+hex(instance))
		plot_pdr.add_subplot(instance_to_points[instance][0], instance_to_points[instance][3], "instance "+hex(instance))
	plotter.plot(plot_hopcount, y_limit=[0,10], folder="2")
	plotter.plot(plot_latency, y_limit =[0, 5], folder="2")
	plotter.plot(plot_pdr, y_limit=[0,1], folder="2")




def get_avg_hopcount(datagrams):
	datagrams = filter(lambda x: x.latency() != Packet.INFINITE_TIME, datagrams)

	if len(datagrams) == 0:
		return 0

	sum_hopcount = reduce(lambda x,y: x+y, map(lambda x: x.hopcount(), datagrams))

	return float(sum_hopcount) / len(datagrams)

def get_avg_latency(datagrams):
	datagrams = filter(lambda x: x.latency() != Packet.INFINITE_TIME, datagrams)

	if len(datagrams) == 0:
		return 0

	sum_latency = reduce(lambda x,y: x+y, map(lambda x: x.latency(), datagrams))

	return float(sum_latency) / len(datagrams)

def get_pdr(datagrams):
	packet_sent = len(datagrams)
	datagrams = filter(lambda x: x.latency() != Packet.INFINITE_TIME, datagrams)
	packets_received = len(datagrams)

	if len(datagrams) == 0:
		return 0.0

	return float(packets_received) / packet_sent

analyse()