/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/uip.h"
#include "net/rpl/rpl.h"
#include "node-id.h"

#include "net/netstack.h"
#include "dev/button-sensor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define UDP_CLIENT_PORT	8765
#define UDP_SERVER_PORT	5678

#define UDP_EXAMPLE_ID  190

static struct uip_udp_conn *server_conn;

PROCESS(udp_server_process, "UDP server process");
AUTOSTART_PROCESSES(&udp_server_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{
  char *appdata;

  if(uip_newdata()) {
    appdata = (char *)uip_appdata;
    appdata[uip_datalen()] = 0;
    PRINTF("app:recv:%u:", UIP_IP_BUF->srcipaddr.u8[sizeof(UIP_IP_BUF->srcipaddr.u8) - 1]);
    PRINTF("%s\n", appdata);
#if SERVER_REPLY
    PRINTF("DATA sending reply\n");
    uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
    uip_udp_packet_send(server_conn, "Reply", sizeof("Reply"));
    uip_create_unspecified(&server_conn->ripaddr);
#endif
  }
}
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(state == ADDR_TENTATIVE || state == ADDR_PREFERRED) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
      /* hack to make address "final" */
      if (state == ADDR_TENTATIVE) {
	uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
      }
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
rpl_set_metric(rpl_instance_t *instance, int mc_routing_type)
{
  rpl_metric_object_t *metric_object;

  metric_object = rpl_find_metric_any_routing_type(&instance->mc, RPL_DAG_MC_METRIC_OBJECT);
  if(metric_object == NULL){
    metric_object = rpl_alloc_metric(&instance->mc);
    if(metric_object == NULL){
      PRINTF("RPL: Cannot update the metric container, no metric object available\n");
      return;
    }
    metric_object->type = mc_routing_type;
    metric_object->flags = RPL_DAG_MC_FLAG_P;
    metric_object->aggr = RPL_DAG_MC_AGGR_ADDITIVE;
    metric_object->prec = 0;
    metric_object->length = 2;
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_server_process, ev, data)
{
  // Variables definition
uip_ipaddr_t ipaddr1, ipaddr2;
struct uip_ds6_addr *root_if1, *root_if2;
rpl_of_t *of1, *of2;
rpl_instance_t *instance1, *instance2;
uint8_t instance_id1, instance_id2;

  PROCESS_BEGIN();

  PROCESS_PAUSE();

  SENSORS_ACTIVATE(button_sensor);

  PRINTF("UDP server started\n");

#if UIP_CONF_ROUTER

  // Instances initialization
instance_id1 = 10;
instance1 = rpl_alloc_instance(instance_id1);
uip_ip6addr(&ipaddr1, UIP_DS6_DEFAULT_PREFIX,0,0,0,0,0,0,1);
uip_ds6_addr_add(&ipaddr1, 0, ADDR_MANUAL);
root_if1 = uip_ds6_addr_lookup(&ipaddr1);
of1 = rpl_find_of(RPL_OCP_OF0);
if(root_if1 != NULL) {
  rpl_dag_t *dag1;
  rpl_set_of(instance_id1, of1);
  dag1 = rpl_set_root(instance_id1,(uip_ip6addr_t *)&ipaddr1);
  uip_ip6addr(&ipaddr1, UIP_DS6_DEFAULT_PREFIX,0,0,0,0,0,0,0);
  rpl_set_prefix(dag1, &ipaddr1, 64);
  PRINTF("created a new RPL dag\n");
} else {
  PRINTF("failed to create a new RPL DAG\n");
}
instance_id2 = 20;
instance2 = rpl_alloc_instance(instance_id2);
uip_ip6addr(&ipaddr2, UIP_DS6_DEFAULT_PREFIX,0,0,0,0,0,0,1);
uip_ds6_addr_add(&ipaddr2, 0, ADDR_MANUAL);
root_if2 = uip_ds6_addr_lookup(&ipaddr2);
of2 = rpl_find_of(RPL_OCP_MRHOF);
if(root_if2 != NULL) {
  rpl_dag_t *dag2;
  rpl_set_of(instance_id2, of2);
  dag2 = rpl_set_root(instance_id2,(uip_ip6addr_t *)&ipaddr2);
  uip_ip6addr(&ipaddr2, UIP_DS6_DEFAULT_PREFIX,0,0,0,0,0,0,0);
  rpl_set_prefix(dag2, &ipaddr2, 64);
  PRINTF("created a new RPL dag\n");
} else {
  PRINTF("failed to create a new RPL DAG\n");
}

#endif /* UIP_CONF_ROUTER */
  
  print_local_addresses();

  /* The data sink runs with a 100% duty cycle in order to ensure high 
     packet reception rates. */
  NETSTACK_MAC.off(1);

  server_conn = udp_new(NULL, UIP_HTONS(UDP_CLIENT_PORT), NULL);
  if(server_conn == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(server_conn, UIP_HTONS(UDP_SERVER_PORT));

  PRINTF("Created a server connection with remote address ");
  PRINT6ADDR(&server_conn->ripaddr);
  PRINTF(" local/remote port %u/%u\n", UIP_HTONS(server_conn->lport),
         UIP_HTONS(server_conn->rport));

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
